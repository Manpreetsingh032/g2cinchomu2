package `in`.his.g2cinchomu2

import `in`.his.g2cinchomu2.*
import `in`.his.g2cinchomu2.apiinterface.ApiService
import `in`.his.g2cinchomu2.apiinterface.RetroClient
import `in`.his.g2cinchomu2.dataModel.InsertModel
import `in`.his.g2cinchomu2.dataModel.Password
import `in`.his.g2cinchomu2.dataModel.ProductionItem
import `in`.his.g2cinuk.ClickObjHandle
import `in`.his.g2cinuk.HandleClick
import android.annotation.SuppressLint
import android.app.Dialog
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_new_pallets.*
import kotlinx.android.synthetic.main.chomu.*
import kotlinx.android.synthetic.main.search.*
import kotlinx.android.synthetic.main.select_line.*
import kotlinx.android.synthetic.main.spinner_dialog_block.*
import kotlinx.android.synthetic.main.spinner_dialog_sku.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MainActivity : AppCompatActivity(), TextToSpeech.OnInitListener {
    private lateinit var insertNewDialog: Dialog
    private lateinit var lineSelectionDialog: Dialog
    private lateinit var skuDialog: Dialog
    private lateinit var blockDialog: Dialog
    private var selectedLine = ""
    private var selectedSku = ""
    private var selectedBatch = ""
    private var currentSkuPalletQty = 0.0
    private var totalLineProduction = 0
    var searchSpinnerItem = ""
    var keySearch = ""
    lateinit var searchAdapter: SearchAdapter
    var lineSelected = false
    private var tts1: TextToSpeech? = null
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    private var mAudioManager: AudioManager? = null
    lateinit var mqtt: MQtt
//    lateinit var viewPager: ViewPager
    lateinit var clickObjHandle: ClickObjHandle
    private lateinit var handleClick: HandleClick

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        clickObjHandle = ClickObjHandle.instance()
        clickObjHandle.addClick(object : HandleClick {
            override fun clickBay(bay: String) {
                val list = Search().searchFromBay(bay)
                searchAdapter.setList(list)
                tts(list)
            }
        })
        ViewListener.instance().addBayView(object : SetBayView {
            override fun changeView() {
                findEmptyBay()
            }
        })

        this.handleClick = ClickObjHandle.instance().handleClick

       /* val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tab_layout)
        tabs.setupWithViewPager(viewPager)

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

        })
*/
        //Initialize TTS
        tts1 = TextToSpeech(this, this)
        tts1!!.language = Locale.ENGLISH
        tts1!!.setSpeechRate(.70.toFloat())
        mqtt = MQtt.managerInstance

        mqtt.mQTTConnect(this, resources.getString(R.string.app_name))

        mqttCallings()

        mAudioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        setDialogs()
        buttonClicks()
        try {
            searchAdapter = SearchAdapter()
            val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rv_show_data_selected_bay.layoutManager = linearLayoutManager
            rv_show_data_selected_bay.adapter = searchAdapter
        } catch (e: Exception) {
        }

        pullToRefresh.setOnRefreshListener {
            try {
                pullToRefresh.isRefreshing = false
                Utils().getAllData()
                Utils().getPlan(this)
                ViewListener.instance().changeView()
//                findEmptyBay()
            } catch (e: Exception) {
            }
        }
//        findEmptyBay()
    }

    private fun mqttCallings() {
        mqtt.addConnectionCalls(object : ConfirmConnection {
            override fun onSuccess() {
                try {
                    mqtt.subscribeTopic(this@MainActivity, "location")
                    mqtt.subscribeTopic(this@MainActivity, "status")
                    mqtt.subscribeTopic(this@MainActivity, "plan")
                } catch (e: Exception) {
                }
            }

            override fun onUnSuccess() {
                try {
                    Handler(Looper.getMainLooper()).postDelayed({
                        mqtt.mQTTConnect(
                            this@MainActivity,
                            resources.getString(R.string.app_name)
                        )
                    }, 30000)
                } catch (e: Exception) {
                    mqtt.mQTTConnect(
                        this@MainActivity,
                        resources.getString(R.string.app_name)
                    )
                }
            }
        })

        mqtt.setValueListener(object : MqttValues {
            override fun onStatus(status: String) {

            }

            override fun onLocation(location: String) {
//                mqtt.publish("location", "DataChange".toByteArray())
                try {
                    if (location == "DataChange") {
                        Utils().getAllData()
                        ViewListener.instance().changeView()
//                        findEmptyBay()
                    } else {
                        Utils().getAllData()
//                        findEmptyBay()
                    }
                } catch (e: Exception) {
                }
            }

            override fun onOthers(topic: String, string: String) {
                try {
                    if (topic == "plan")
                        Utils().getPlan(this@MainActivity)
                } catch (e: Exception) {
                }
            }

        })
    }

    private fun setDialogs() {
        insertNewDialog = Dialog(this)
        with(insertNewDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.add_new_pallets)
            window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(false)
            setCancelable(false)

            submit_new_pallet?.setOnClickListener {
                if (lineSelected) {
                    if (current_qty.text.isNotEmpty()) {
                        try {
                            currentSkuPalletQty = current_qty.text.toString().toDouble()
                            submitNewPallet(current_bay_no.text.toString())
                        } catch (e: Exception) {
                        }
                    } else {
                        try {
                            submitNewPallet(current_bay_no.text.toString())
                        } catch (e: Exception) {
                        }
                    }
                } else {
                    try {
                        currentSkuPalletQty = current_qty.text.toString().toDouble()
                        selectedSku = current_sku.text.toString()
                        selectedBatch = batch_no.text.toString()
                        submitNewPallet(current_bay_no.text.toString())
                    } catch (e: Exception) {
                    }
                }
            }

            cancel_new_pallet?.setOnClickListener { dismiss() }

            bay_list_spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    current_bay_no.text = bay_list_spinner.selectedItem.toString()
                }

            }
        }

        lineSelectionDialog = Dialog(this)
        with(lineSelectionDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.select_line)
            window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(false)
            setCancelable(false)

            submit_line.setOnClickListener {
                try {
                    if (spinner_select_line.selectedItem != null) {
                        selectedLine = spinner_select_line.selectedItem?.toString()!!
                        Utils.proPlan.forEach {
                            if (selectedLine.split(":")[0].trim() == it?.lineNo
                                && selectedLine.split(":")[1].trim() == it.sku
                                && selectedLine.split(":")[2].trim() == it.batchNo
                            ) {
                                totalLineProduction = it.qty!!
                            }
                        }
                        dismiss()
                        selectAccLine()
                    }
                } catch (e: Exception) {
                }
            }

            cancel_line.setOnClickListener { dismiss() }

        }

        skuDialog = Dialog(this)
        with(skuDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.spinner_dialog_sku)
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(true)
            setCancelable(true)

            try {
                val adapter: ArrayAdapter<String> = ArrayAdapter(
                    this@MainActivity,
                    android.R.layout.simple_list_item_1, android.R.id.text1, Utils.skusList
                )
                list_sku.adapter = adapter
                list_sku.onItemClickListener =
                    AdapterView.OnItemClickListener { _: AdapterView<*>?, _: View?, position: Int, _: Long ->
                        val skuItem = adapter.getItem(position)
                        Log.d("TAG", "init: $skuItem")
                        skuDialog.dismiss()
                        val list = Search().getSkuItem(skuItem)
                        searchAdapter.setList(list)
                        tts(list)
                        tv_search?.text = skuItem
                    }
            } catch (e: Exception) {
            }
        }

        blockDialog = Dialog(this)
        with(blockDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.spinner_dialog_block)
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(true)
            setCancelable(true)

            block_a?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("A")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block_a)
                    dismiss()
                } catch (e: Exception) {
                }
            }
            block_B?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("B")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block__b)
                    dismiss()
                } catch (e: Exception) {
                }
            }
            block_C?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("C")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block__c)
                    dismiss()
                } catch (e: Exception) {
                }
            }
        }

        try {
            val searchAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.search_spinner, R.layout.support_simple_spinner_dropdown_item
            )
            searchAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            search_spinner?.adapter = searchAdapter
        } catch (e: Exception) {
        }
    }

    private fun selectAccLine() {
        lineSelected = true
        for (i in Utils.proPlan) {
            try {
                if (i!!.lineNo == selectedLine.split(":")[0].trim() &&
                    i.sku == selectedLine.split(":")[1].trim() &&
                    i.batchNo == selectedLine.split(":")[2].trim()
                ) {
                    selectedBatch = i.batchNo.toString()
                    selectedSku = i.sku
                }
            } catch (e: Exception) {
            }
        }

        for (i in Utils.skuList) {
            try {
                if (i!!.sku == selectedSku) {
                    currentSkuPalletQty = i.casesOfPallets!!
                }
            } catch (e: Exception) {
            }
        }
    }

    private fun submitNewPallet(bayNo: String) {
        try {
            val insertModel = InsertModel(
                selectedSku, selectedBatch,
                currentSkuPalletQty.toInt().toString(), bayNo, "PASS"
            )
            val call =
                apiService.INSERT_RESPONSE_CALL(selectedLine.split(":")[0].trim(), insertModel)

            call.enqueue(object : Callback<Password> {
                override fun onFailure(call: Call<Password>, t: Throwable) {
                    Toast.makeText(
                        this@MainActivity,
                        "Please Check Network Connection First", Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(call: Call<Password>, response: Response<Password>) {
                    if (response.isSuccessful) {
                        when {
                            response.body()?.message.isNullOrBlank() -> {
                                Toast.makeText(
                                    this@MainActivity,
                                    "There is some Problem, Please Reconnect to Server",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            else -> {
                                when (response.body()!!.message) {
                                    "Successful" -> {
                                        Toast.makeText(
                                            this@MainActivity,
                                            " " + response.body()!!.message,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        Utils().getPlan(this@MainActivity)
                                        mqtt.publish("location", "DataChange".toByteArray())
                                    }
                                    "Limit Exceeded" -> {
                                        Toast.makeText(
                                            this@MainActivity,
                                            "Production Completed.Please Select Next Plan",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        Utils().getPlan(this@MainActivity)
                                    }
                                    else -> {
                                        Toast.makeText(
                                            this@MainActivity,
                                            "There is some Problem, Please Try After Some Time ",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }

    }

    @SuppressLint("SetTextI18n")
    private fun buttonClicks() {
        insert_new?.setOnClickListener {
            if (selectedLine.isNotBlank()) {
                insertNewDialog.show()
                val skuAdapter = ArrayAdapter(
                    this,
                    R.layout.support_simple_spinner_dropdown_item,
                    Utils.skusList
                )
                skuAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                insertNewDialog.sku_list_spinner.adapter = skuAdapter

                val bayAdapter = ArrayAdapter(
                    this,
                    R.layout.support_simple_spinner_dropdown_item, Utils.baysList
                )
                bayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                insertNewDialog.bay_list_spinner.adapter = bayAdapter
                if (selectedLine.isNotEmpty()) {
                    insertNewDialog.current_sku.text = selectedSku
                    insertNewDialog.batch_no.setText(selectedBatch)
                    insertNewDialog.batch_no.isEnabled = false
                    insertNewDialog.current_line_no.text =
                        getString(R.string.selected_line) + ": " + selectedLine.split(":")[0].trim() + "  Total Production: " + totalLineProduction
                    insertNewDialog.sku_list_spinner.isEnabled = false
                } else {
                    insertNewDialog.current_line_no.text =
                        "if you want work acc. to plan please select line first"
                    insertNewDialog.batch_no.isEnabled = true
                    insertNewDialog.sku_list_spinner.isEnabled = true
                }
            } else {
                showLine()
            }
        }

        insertNewDialog.sku_list_spinner?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if (!lineSelected) {
                        insertNewDialog.current_sku.text =
                            insertNewDialog.sku_list_spinner.selectedItem.toString()
                    }
                }
            }

        more_options?.setOnClickListener {
            showLine()
        }

        b8?.setOnClickListener {
            performClick("B-8")
        }
        b7?.setOnClickListener {
            performClick("B-7")
        }
        b6?.setOnClickListener {
            performClick("B-6")
        }
        b5?.setOnClickListener {
            performClick("B-5")
        }
        b4?.setOnClickListener {
            performClick("B-4")
        }
        b3?.setOnClickListener {
            performClick("B-3")
        }
        b2?.setOnClickListener {
            performClick("B-2")
        }
        b1?.setOnClickListener {
            performClick("B-1")
        }
        forklift?.setOnClickListener {
            performClick("FORKLIFT-PATH")
        }


        btl_fifo?.setOnClickListener {
            searchAdapter.sortFifo()

        }
        btl_lifo?.setOnClickListener {
            searchAdapter.sortLifo()
        }

        tts_on?.setOnClickListener {
            tts_off?.visibility = View.VISIBLE
            tts_on?.visibility = View.GONE
            try {
                if (tts1!!.isSpeaking) {
                    tts1!!.stop()
                    val fullSetVolume = 0
                    mAudioManager?.setStreamVolume(AudioManager.STREAM_MUSIC, fullSetVolume, 0)
                }
            } catch (e: java.lang.Exception) {
                Log.d("TAG", "onClick: $e")
            }
        }

        tts_off?.setOnClickListener {
            tts_off?.visibility = View.GONE
            tts_on?.visibility = View.VISIBLE
            try {
                val setVolume = 10
                mAudioManager?.setStreamVolume(AudioManager.STREAM_MUSIC, setVolume, 0)
            } catch (e: java.lang.Exception) {
                Log.d("TAG", "onClick: $e")
            }


        }

        et_search?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                keySearch = p0!!.toString()
                when (searchSpinnerItem) {
                    "Batch No" -> {
                        val list = Search().searchFromBatch(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                    "Bay No" -> {
                        val list = Search().searchFromBay(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                    "Select…" -> {
                        val list = Search().searchFromAll(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                }
            }
        })

        search_spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                searchSpinnerItem = search_spinner.selectedItem.toString()

                Log.d("TAG", "search_spinner$searchSpinnerItem")
                when (searchSpinnerItem) {
                    "Select…" -> {
                        et_search.isEnabled = true
                    }
                    "All" -> {
                        val list = Search().searchFromAllExceptEmpty("empty")
                        searchAdapter.setList(list)
                        tts(list)
                        tv_search.text = "All Items"
                        et_search.isEnabled = false
                    }
                    "SKU" -> {
                        et_search.isEnabled = false
                        skuDialog.show()
                    }
                    "Block" -> {
                        et_search.isEnabled = false
                        blockDialog.show()
                    }
                    "Bay No" -> {
                        et_search.isEnabled = true
                        searchSpinnerItem = search_spinner.selectedItem.toString()
                        tv_search.text = "From BAY"
                    }
                    "Batch No" -> {
                        et_search.isEnabled = true
                        searchSpinnerItem = search_spinner.selectedItem.toString()
                        tv_search.text = "From Batch"
                    }
                }
            }
        }
    }

    private fun performClick(name: String) {
        if (this::handleClick.isInitialized) {
            handleClick.clickBay(name)
        }
    }
    private fun showLine() {
        try {
            lineSelectionDialog.show()
            val lineAdapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, Utils.lineNos
            )
            lineAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            lineSelectionDialog.spinner_select_line.adapter = lineAdapter
        } catch (e: Exception) {
        }
    }

    fun tts(list: ArrayList<ProductionItem>) {
        var speechtext: String
        if (tts1!!.isSpeaking) {
            tts1!!.stop()
        }
        try {
            for (i in list.indices) {
                speechtext = "bay " + java.lang.String.valueOf(list[i].bayNo) +
                        ",S.K.U " + java.lang.String.valueOf(list[i].sku) +
                        ",Batch " + java.lang.String.valueOf(list[i].batchNo) +
                        ",Quantity " + java.lang.String.valueOf(list[i].qty)
                tts1!!.speak(speechtext, TextToSpeech.QUEUE_ADD, null, "")
            }
        } catch (e: java.lang.Exception) {
            Log.e("TAG", "tts: ", e)
        }
    }

    override fun onInit(status: Int) {
        try {
            if (status == TextToSpeech.SUCCESS) {
                val result: Int = tts1?.setLanguage(Locale.US)!!
                if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED
                ) {
                    Log.d("textToSpeech", "This Language is not supported")
                } else {
                    Log.d("TAG", "onInit: success")
                }
            } else {
                Log.d("textToSpeech", "Initialization Failed!")
            }
        } catch (e: Exception) {
            Log.d("TAG", "onInit: $e")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (tts1?.isSpeaking!!) {
            tts1?.stop()
            tts1?.shutdown()
        }
    }

    private fun findEmptyBay() {

        setBack("B-8", b8)
        setBack("B-7", b7)
        setBack("B-6", b6)
        setBack("B-5", b5)
        setBack("B-4", b4)
        setBack("B-3", b3)
        setBack("B-2", b2)
        setBack("B-1", b1)

    }
    private fun setBack(name: String, layout: TextView) {
        try {
            val list = Search().searchFromBay(name)
            if (list[0].sku.equals("empty", true)) {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
            } else {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
            }
        } catch (e: Exception) {
            Log.d("TAG", "setBack: $e")
        }
    }

}