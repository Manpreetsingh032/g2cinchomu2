package in.his.g2cinchomu2.apiinterface;

import in.his.g2cinchomu2.dataModel.Password;
import in.his.g2cinchomu2.dataModel.BayResponse;
import in.his.g2cinchomu2.dataModel.InsertModel;
import in.his.g2cinchomu2.dataModel.ProductionAllResponse;
import in.his.g2cinchomu2.dataModel.ProductionPlanResponse;
import in.his.g2cinchomu2.dataModel.SkuListResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    /*http://192.168.43.106:8082/login*/

    @GET("getLogin/")
    Call<Password> PASSWORD_CALL(@Query("user_name") String user_name,
                                 @Query("password") String password);

    @GET("getSkuListData/")
    Call<SkuListResponse> SKU_LIST_RESPONSE_CALL();

    @GET("getAllProductionData/")
    Call<ProductionAllResponse> PRODUCTION_ALL_RESPONSE_CALL();

    @POST("insertProduction/")
    Call<Password> INSERT_RESPONSE_CALL(@Query("line_no") String line_no, @Body InsertModel insertModel);

    @GET("getTodayProductionPlan/")
    Call<ProductionPlanResponse> PRODUCTION_PLAN_RESPONSE_CALL();

    @GET("getBayList/")
    Call<BayResponse> BAY_RESPONSE_CALL();
}
