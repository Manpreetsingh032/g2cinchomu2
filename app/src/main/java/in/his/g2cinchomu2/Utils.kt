package `in`.his.g2cinchomu2

import `in`.his.g2cinchomu2.apiinterface.ApiService
import `in`.his.g2cinchomu2.apiinterface.RetroClient
import `in`.his.g2cinchomu2.dataModel.*
import android.content.Context
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Utils {

    fun getPlan(c: Context) {
        try {
            val call = apiService.PRODUCTION_PLAN_RESPONSE_CALL()
            call.enqueue(object : Callback<ProductionPlanResponse> {
                override fun onFailure(call: Call<ProductionPlanResponse>, t: Throwable) {
                    Toast.makeText(c,
                            "Please Check Network Connection First", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<ProductionPlanResponse>, response: Response<ProductionPlanResponse>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            if (response.body()!!.proplan!!.isNotEmpty()) {
                                proPlan = emptyList()
                                proPlan = response.body()!!.proplan!!
                                lineNos.clear()
                                for (i in proPlan) {
                                    lineNos.add("${i!!.lineNo!!}:${i.sku}:${i.batchNo}")
                                }
                            } else {
                                Toast.makeText(c, "There is no working plan available,Please contact to administrator",
                                        Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getSku(c: Context) {
        try {
            val call: Call<SkuListResponse> = LoginActivity.apiservice.SKU_LIST_RESPONSE_CALL()
            call.enqueue(object : Callback<SkuListResponse?> {
                override fun onResponse(call: Call<SkuListResponse?>, response: Response<SkuListResponse?>) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.skuData!!.isNotEmpty()) {
                            skusList.clear()
                            skuList = response.body()!!.skuData!!
                            skusList.clear()
                            for (i in skuList) {
                                skusList.add(i?.sku!!)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<SkuListResponse?>, t: Throwable) {
                    Toast.makeText(c, "Please Check Network Connection First",
                            Toast.LENGTH_SHORT).show()
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getBay() {
        try {
            val call = apiService.BAY_RESPONSE_CALL()
            call.enqueue(object : Callback<BayResponse> {
                override fun onFailure(call: Call<BayResponse>, t: Throwable) {

                }

                override fun onResponse(call: Call<BayResponse>, response: Response<BayResponse>) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.bay!!.isNotEmpty()) {
                            bayList = emptyList()
                            bayList = response.body()!!.bay!!
                            baysList.clear()
                            for (i in bayList) {

                                baysList.add(i!!.bay!!)
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getAllData() {
        try {
            val call = apiService.PRODUCTION_ALL_RESPONSE_CALL()
            call.enqueue(object : Callback<ProductionAllResponse> {
                override fun onFailure(call: Call<ProductionAllResponse>, t: Throwable) {

                }

                override fun onResponse(call: Call<ProductionAllResponse>, response: Response<ProductionAllResponse>) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.production.isNullOrEmpty() -> {
                            }
                            else -> {
                                allData = emptyList()
                                allData = response.body()!!.production!!
                                Log.d("ListSize", allData.size.toString())
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    companion object {
        var allData: List<ProductionItem?> = ArrayList<ProductionItem>()
        var proPlan: List<ProplanItem?> = ArrayList<ProplanItem>()
        var skuList: List<SkuDataItem?> = ArrayList<SkuDataItem>()
        var bayList: List<BayItem?> = ArrayList<BayItem>()
        var skusList = ArrayList<String>()
        var baysList = ArrayList<String>()
        var lineNos = ArrayList<String>()
        private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    }
}